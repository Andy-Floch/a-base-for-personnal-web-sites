# Generated by Django 2.1.5 on 2019-01-30 17:07

import blog.models
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='article',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('article_title', models.CharField(max_length=200, verbose_name='Titre')),
                ('article_picture', models.ImageField(blank=True, max_length=500, null=True, upload_to='img', verbose_name='Image')),
                ('article_thumbnail', models.ImageField(blank=True, max_length=500, null=True, upload_to='img', verbose_name='Miniature')),
                ('article_content', models.TextField(verbose_name='Contenu')),
                ('article_pub_date', models.DateTimeField(verbose_name='Date de publication')),
                ('article_author', models.ForeignKey(on_delete=models.SET(blog.models.get_sentinel_user), to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
