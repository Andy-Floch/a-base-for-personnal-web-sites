from django.conf import settings
from django.db import models
from django.contrib.auth import get_user_model
from django.urls import reverse
from os.path import join
from config.settings import MEDIA_IMAGES_DIR


def get_sentinel_user():
    return get_user_model().objects.get_or_create(username='Anonyme')[0]


class article(models.Model):
    article_title = models.CharField(max_length=200, verbose_name='Titre')
    article_picture = models.ImageField(
        max_length=500,
        upload_to=join(MEDIA_IMAGES_DIR),
        blank=True,
        null=True,
        verbose_name='Image'
    )
    article_thumbnail = models.ImageField(
        max_length=500,
        upload_to=join(MEDIA_IMAGES_DIR),
        blank=True,
        null=True,
        verbose_name='Miniature'
    )
    article_author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.SET(get_sentinel_user)
    )
    article_content = models.TextField(verbose_name='Contenu')
    article_pub_date = models.DateTimeField('Date de publication')

    def get_absolute_url(self):
        return reverse('blog:article_detail', kwargs={'pk': self.pk})

# TODO: Class comment
