#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.mixins import LoginRequiredMixin
from django.forms import Textarea
from django.urls import reverse_lazy, reverse
from django.utils import timezone
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import article


class IndexView(ListView):
    template_name = 'blog/articles.html'
    context_object_name = 'article_list'

    def get_queryset(self):
        article_list = article.objects.order_by('-article_pub_date')
        paginator = Paginator(article_list, 10)  # Show 10 articles per page

        page = self.request.GET.get('page', 1)
        try:
            articles = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            articles = paginator.page(1)
        except EmptyPage:
            # If page is out of range, deliver last page of results.
            articles = paginator.page(paginator.num_pages)
        return articles


class DetailView(DetailView):
    model = article
    template_name = 'blog/article.html'


class ArticleCreate(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = article
    template_name_suffix = '_add'
    login_url = '/accounts/login/'
    fields = ['article_title', 'article_picture', 'article_content']
    widgets = {
        'article_content': Textarea(),
    }

    success_message = "L'article a été ajouté avec succès !"

    def get_success_url(self):
        return reverse('blog:article_detail', kwargs={'pk': self.object.pk})

    def form_valid(self, form):
        form.instance.article_pub_date = timezone.now()
        form.instance.article_author = self.request.user
        return super(ArticleCreate, self).form_valid(form)


class ArticleUpdate(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    login_url = '/accounts/login/'
    model = article
    template_name_suffix = '_update'
    fields = ['article_title', 'article_picture', 'article_content']
    widgets = {
        'article_content': Textarea(),
    }

    success_message = "L'article a été mis à jour avec succès !"

    def get_success_url(self):
        return reverse('blog:article_detail', kwargs={'pk': self.object.pk})


class ArticleDelete(LoginRequiredMixin, SuccessMessageMixin, DeleteView):
    login_url = '/accounts/login/'
    model = article
    success_message = "L'article a été supprimé avec succès !"
    success_url = reverse_lazy('index')
