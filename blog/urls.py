from django.urls import path

from . import views

app_name = 'blog'
urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('<int:pk>)/', views.DetailView.as_view(), name='article_detail'),
    path('add/', views.ArticleCreate.as_view(), name='article_add'),
    path('edit/<int:pk>/', views.ArticleUpdate.as_view(), name='article_edit'),
    path('delete/<int:pk>/', views.ArticleDelete.as_view(), name='article_delete'),
]
